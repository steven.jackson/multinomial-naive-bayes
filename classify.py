import math
import operator
import argparse
import os


def tf(training_data, testing_data, verbose=True):
    # Open file to read test data
    training_data_fp = open(training_data, 'r')

    # Read in test data storing it in dictionary
    dictionary = dict()
    buffer = training_data_fp.readline()
    while len(buffer) != 0:
        data = buffer.split()
        class_num = int(data.pop(0))
        for word in data:
            if word in dictionary:
                dictionary[word][class_num] += 1
            else:
                if class_num == 1:
                    dictionary[word] = [word, 1, 0]
                else:
                    dictionary[word] = [word, 0, 1]
        buffer = training_data_fp.readline()

    # Get top five most occurring terms in each class and print them to screen
    class_data = [None, [], []]
    class_top_five = [None, [], []]
    class_data[1] = sorted(dictionary.values(), key=operator.itemgetter(1), reverse=True)
    class_top_five[1] = class_data[1][:5]
    class_data[-1] = sorted(dictionary.values(), key=operator.itemgetter(2), reverse=True)
    class_top_five[-1] = class_data[-1][:5]

    # Print results
    if verbose:
        print('Class 1: [word, class 1 count, class -1 count]')
        print(class_top_five[1])
        print('\nClass -1: [word, class 1 count, class -1 count]')
        print(class_top_five[-1])

    # Write dictionaries to file named tf.csv
    tf_csv_fp = open('tf.csv', 'w+')
    for term in dictionary.values():
        tf_csv_fp.write(term[0] + ', ' + str(term[1]) + ', ' + str(term[2]) + '\n')

    # Close file pointers
    training_data_fp.close()
    tf_csv_fp.close()


def tfgrep(training_data, testing_data, verbose=True):
    # Find most discriminating term
    if not os.path.isfile('tf.csv'):
        tf(training_data, testing_data, verbose=False)
    tf_csv_fp = open('tf.csv', 'r')
    most_discriminating_term = None
    mdt_diff = 0
    for line in tf_csv_fp.readlines():
        data = line.split(', ')
        diff = abs(int(data[1]) - int(data[2]))
        if diff > mdt_diff:
            most_discriminating_term = data
            mdt_diff = diff

    # Open files for testing
    training_data_fp = open(training_data, 'r')
    testing_data_fp = open(testing_data, 'r')

    # Process data
    def process_file(data_fp, _mdt_word, _guess):
        false_positive = 0
        true_positive = 0
        false_negative = 0
        true_negative = 0
        for _line in data_fp.readlines():
            _line = _line.strip()
            _data = _line.split()
            class_actual = int(_data.pop(0))
            _class_guess = _guess
            if _mdt_word not in _data:
                _class_guess *= -1
            if _class_guess == -1 and class_actual == -1:
                true_negative += 1
            elif _class_guess == -1 and class_actual == 1:
                false_negative += 1
            elif _class_guess == 1 and class_actual == -1:
                false_positive += 1
            elif _class_guess == 1 and class_actual == 1:
                true_positive += 1
        return true_negative, false_negative, false_positive, true_positive

    mdt_word, class_one, class_neg_one = most_discriminating_term
    if int(class_one) > int(class_neg_one):
        class_guess = 1
    else:
        class_guess = -1
    train_tn, train_fn, train_fp, train_tp = process_file(training_data_fp, mdt_word, class_guess)
    test_tn,  test_fn,  test_fp,  test_tp  = process_file(testing_data_fp, mdt_word, class_guess)

    # Display results
    if verbose:
        print('Most Discriminating Term: ' + mdt_word + ' ' + class_one + ' ' + class_neg_one)
        print('Training Data:')
        print(' FP: ' + str(train_fp) + '   TN: ' + str(train_tn))
        print(' TP: ' + str(train_tp) + '   FN: ' + str(train_fn))
        train_accuracy = (train_tp + train_tn) / (train_tp + train_tn + train_fp + train_fn)
        print(' Accuracy: ' + str(train_accuracy))
        print('\nTesting Data:')
        print(' FP: ' + str(test_fp) + '    TN: ' + str(test_tn))
        print(' TP: ' + str(test_tp) + '    FN: ' + str(test_fn))
        test_accuracy = (test_tp + test_tn) / (test_tp + test_tn + test_fp + test_fn)
        print(' Accuracy: ' + str(test_accuracy))


def priors(training_data, testing_data, verbose=True):
    # Open file for reading
    training_data_fp = open(training_data, 'r')

    # Read file counting class occurrences
    count_for_class = [None, 0, 0]
    for document in training_data_fp.readlines():
        document = document.strip().split()
        class_num = int(document.pop(0))
        count_for_class[class_num] += 1
    total_class_count = count_for_class[1] + count_for_class[-1]
    class_p1_probability = count_for_class[1] / total_class_count
    class_n1_probability = count_for_class[-1] / total_class_count

    # Close file pointers
    training_data_fp.close()

    # Open files for testing
    training_data_fp = open(training_data, 'r')
    testing_data_fp = open(testing_data, 'r')

    # Process documents
    def process_documents(_training_data_fp, class_guess):
        true_negative = 0
        false_negative = 0
        false_positive = 0
        true_positive = 0
        for _document in _training_data_fp.readlines():
            _document = _document.strip().split()
            class_actual = int(_document.pop(0))
            if class_guess == -1 and class_actual == -1:
                true_negative += 1
            elif class_guess == -1 and class_actual == 1:
                false_negative += 1
            elif class_guess == 1 and class_actual == -1:
                false_positive += 1
            elif class_guess == 1 and class_actual == 1:
                true_positive += 1
        return true_positive, false_positive, true_negative, false_negative

    if class_p1_probability > class_n1_probability:
        guess = 1
    else:
        guess = -1
    train_tp, train_fp, train_tn, train_fn = process_documents(training_data_fp, guess)
    test_tp,  test_fp,  test_tn,  test_fn  = process_documents(testing_data_fp, guess)

    # Print results
    if verbose:
        print('Training Data:')
        print(' FP: ' + str(train_fp) + '   TN: ' + str(train_tn))
        print(' TP: ' + str(train_tp) + '   FN: ' + str(train_fn))
        train_accuracy = (train_tp + train_tn) / (train_tp + train_tn + train_fp + train_fn)
        print(' Accuracy: ' + str(train_accuracy))
        print('\nTesting Data:')
        print(' FP: ' + str(test_fp) + '    TN: ' + str(test_tn))
        print(' TP: ' + str(test_tp) + '    FN: ' + str(test_fn))
        test_accuracy = (test_tp + test_tn) / (test_tp + test_tn + test_fp + test_fn)
        print(' Accuracy: ' + str(test_accuracy))


def mnb(training_data, testing_data, verbose=True):
    # Open tf.csv and collect its data
    if not os.path.isfile('tf.csv'):
        tf(training_data, testing_data, verbose=False)
    tf_csv_fp = open('tf.csv', 'r')
    terms_and_counts = tf_csv_fp.readlines()
    tf_csv_fp.close()

    # Get total number of words in each document
    dictionary = dict()
    num_terms = [None, 0, 0]
    for term in terms_and_counts:
        term = term.strip().split(', ')
        dictionary[term[0]] = [term[0], int(term[1]), int(term[2])]
        num_terms[1] += int(term[1])
        num_terms[-1] += int(term[-1])

    # Get class frequency
    with open(training_data, 'r') as training_data_fp:
        training_docs = training_data_fp.readlines()
    class_counts = [None, 0, 0]
    for doc in training_docs:
        doc = doc.strip().split()
        class_counts[int(doc.pop(0))] += 1
    class_prob = [None, 0, 0]
    class_prob[1] = class_counts[1] / (class_counts[1] + class_counts[-1])
    class_prob[-1] = class_counts[-1] / (class_counts[1] + class_counts[-1])

    # Classify document
    with open(testing_data) as testing_data_fp:
        testing_docs = testing_data_fp.readlines()

    def process_documents(documents):
        tp = 0
        tn = 0
        fp = 0
        fn = 0
        for doc in documents:
            prob_document = [None, math.log10(class_prob[1]), math.log10(class_prob[-1])]
            doc = doc.strip().split()
            actual_class = int(doc.pop(0))
            temp_dictionary = dict()
            for term in doc:
                if term not in temp_dictionary:
                    temp_dictionary[term] = 0
                temp_dictionary[term] += 1
            for temp_term in temp_dictionary:
                term_prob_in_class = [None, 0, 0]
                if temp_term in dictionary:
                    term_prob_in_class[1] = dictionary[temp_term][1] + 1
                    term_prob_in_class[-1] = dictionary[temp_term][-1] + 1
                else:
                    term_prob_in_class[1] = 1
                    term_prob_in_class[-1] = 1
                term_prob_in_class[1] /= num_terms[1] + 1
                term_prob_in_class[1] = math.log10(term_prob_in_class[1])
                term_prob_in_class[1] *= temp_dictionary[temp_term]
                term_prob_in_class[-1] /= num_terms[-1] + 1
                term_prob_in_class[-1] = math.log10(term_prob_in_class[-1])
                term_prob_in_class[-1] *= temp_dictionary[temp_term]
                prob_document[1] += term_prob_in_class[1]
                prob_document[-1] += term_prob_in_class[-1]
            if prob_document[1] > prob_document[-1]:
                guess_class = 1
            else:
                guess_class = -1

            if guess_class == -1 and actual_class == -1:
                tn += 1
            elif guess_class == -1 and actual_class == 1:
                fn += 1
            elif guess_class == 1 and actual_class == -1:
                fp += 1
            elif guess_class == 1 and actual_class == 1:
                tp += 1
        return tp, fp, tn, fn

    train_tp, train_fp, train_tn, train_fn = process_documents(training_docs)
    test_tp,  test_fp,  test_tn,  test_fn  = process_documents(testing_docs)

    # Print results
    if verbose:
        print('Training Data:')
        print(' FP: ' + str(train_fp) + '   TN: ' + str(train_tn))
        print(' TP: ' + str(train_tp) + '   FN: ' + str(train_fn))
        train_accuracy = (train_tp + train_tn) / (train_tp + train_tn + train_fp + train_fn)
        print(' Accuracy: ' + str(train_accuracy))
        print('\nTesting Data:')
        print(' FP: ' + str(test_fp) + '    TN: ' + str(test_tn))
        print(' TP: ' + str(test_tp) + '    FN: ' + str(test_fn))
        test_accuracy = (test_tp + test_tn) / (test_tp + test_tn + test_fp + test_fn)
        print(' Accuracy: ' + str(test_accuracy))


def df(training_data, testing_data, verbose=True):
    # Open training file get documents and close file pointer
    _training_data_fp = open(training_data)
    documents = _training_data_fp.readlines()
    _training_data_fp.close()

    # Process training data
    dictionary = dict()
    for document in documents:
        document = document.strip().split()
        class_number = int(document.pop(0))
        running_list = dict()
        for term in document:
            if term not in running_list:
                running_list[term] = term
                if term not in dictionary:
                    dictionary[term] = [term, 0, 0]
                dictionary[term][class_number] += 1

    # Calculate top five occurrences for each class
    top_five_class_p1 = sorted(dictionary.values(), key=operator.itemgetter(1), reverse=True)[:5]
    top_five_class_n1 = sorted(dictionary.values(), key=operator.itemgetter(2), reverse=True)[:5]

    # Open df.csv and save data to file, then close file pointer
    df_csv_fp = open('df.csv', 'w+')
    for term in dictionary.values():
        df_csv_fp.write(term[0] + ', ' + str(term[1]) + ', ' + str(term[2]) + '\n')
    df_csv_fp.close()

    # Print results
    if verbose:
        print('Class 1 top five:')
        print(top_five_class_p1)
        print('\nClass -1 top five:')
        print(top_five_class_n1)


def nb(training_data, testing_data, verbose=True, df_file_path='df.csv'):
    # Training Data:
    # FP: 67
    # TN: 1317
    # TP: 1269
    # FN: 291
    # Accuracy: 0.8783967391304348

    # Testing Data:
    # FP: 73
    # TN: 476
    # TP: 490
    # FN: 160
    # Accuracy: 0.8056713928273561

    # Open files to get documents
    with open(training_data) as training_data_fp:
        training_documents = training_data_fp.readlines()
    with open(testing_data) as testing_data_fp:
        testing_documents = testing_data_fp.readlines()

    # Open df.csv and put in df_dictionary
    if not os.path.isfile(df_file_path):
        df(training_data, testing_data, verbose=False)
    df_dictionary = dict()
    with open(df_file_path, 'r') as df_csv_fp:
        for line in df_csv_fp.readlines():
            line = line.strip().split(', ')
            df_dictionary[line[0]] = [line[0], int(line[1]), int(line[2])]

    # Calculate class count and class probability
    count_for_class = [None, 0, 0]
    for doc in training_documents:
        doc_list = doc.strip().split()
        class_number = int(doc_list.pop(0))
        count_for_class[class_number] += 1
    total_class_count = count_for_class[1] + count_for_class[-1]
    probability_for_class = [None, 0, 0]
    probability_for_class[1] = count_for_class[1] / total_class_count
    probability_for_class[-1] = count_for_class[-1] / total_class_count

    # Process document
    def process_documents(documents):
        tp = 0
        tn = 0
        fp = 0
        fn = 0
        for document in documents:
            prob_document = [None, math.log10(probability_for_class[1]), math.log10(probability_for_class[-1])]
            document = document.strip().split()
            actual_class = int(document.pop(0))
            document_dictionary = dict()
            for document_term in document:
                if document_term not in document_dictionary:
                    document_dictionary[document_term] = None
            for dictionary_term in df_dictionary:
                term_prob_in_class = [None, 0, 0]
                term_prob_in_class[1] = df_dictionary[dictionary_term][1] + 1
                term_prob_in_class[1] /= count_for_class[1] + 2
                term_prob_in_class[-1] = df_dictionary[dictionary_term][-1] + 1
                term_prob_in_class[-1] /= count_for_class[-1] + 2
                if dictionary_term not in document_dictionary:
                    term_prob_in_class[1] = 1 - term_prob_in_class[1]
                    term_prob_in_class[-1] = 1 - term_prob_in_class[-1]
                term_prob_in_class[1] = math.log10(term_prob_in_class[1])
                term_prob_in_class[-1] = math.log10(term_prob_in_class[-1])
                prob_document[1] += term_prob_in_class[1]
                prob_document[-1] += term_prob_in_class[-1]

            # Calculate class guess
            if prob_document[1] > prob_document[-1]:
                guess_class = 1
            else:
                guess_class = -1

            # Update confusion matrix
            if guess_class == -1 and actual_class == -1:
                tn += 1
            elif guess_class == -1 and actual_class == 1:
                fn += 1
            elif guess_class == 1 and actual_class == -1:
                fp += 1
            elif guess_class == 1 and actual_class == 1:
                tp += 1

        # Return results
        return tp, fp, tn, fn

    # Print results
    train_tp, train_fp, train_tn, train_fn = process_documents(training_documents)
    test_tp,  test_fp,  test_tn,  test_fn  = process_documents(testing_documents)

    # Print results
    if verbose:
        print('Training Data:')
        print(' FP: ' + str(train_fp) + '   TN: ' + str(train_tn))
        print(' TP: ' + str(train_tp) + '   FN: ' + str(train_fn))
        train_accuracy = (train_tp + train_tn) / (train_tp + train_tn + train_fp + train_fn)
        print(' Accuracy: ' + str(train_accuracy))
        print('\nTesting Data:')
        print(' FP: ' + str(test_fp) + '    TN: ' + str(test_tn))
        print(' TP: ' + str(test_tp) + '    FN: ' + str(test_fn))
        test_accuracy = (test_tp + test_tn) / (test_tp + test_tn + test_fp + test_fn)
        print(' Accuracy: ' + str(test_accuracy))


def mine(training_data, testing_data, verbose=True):
    # Open files to get documents
    if not os.path.isfile('df.csv'):
        df(training_data, testing_data, verbose=False)
    with open('df.csv', 'r') as document_frequency_fp:
        document_frequency_terms = document_frequency_fp.readlines()

    # Load document frequency terms into a dictionary
    df_dictionary = dict()
    for term in document_frequency_terms:
        term = term.strip().split(', ')
        df_dictionary[term[0]] = [term[0], int(term[1]), int(term[2])]

    # Sort dictionaries and slice off top five percent of most discriminating terms
    df_msd = sorted(df_dictionary.values(), key=lambda t: abs(t[1] - t[2]), reverse=True)[:len(df_dictionary) // 180]

    # Write to file and path names to nb
    with open('df-mdt.csv', 'w') as df_msd_fp:
        for df_term in df_msd:
            df_msd_fp.write(df_term[0] + ', ' + str(df_term[1]) + ', ' + str(df_term[2]) + '\n')

    # Display information about mine
    if verbose:
        print('Improving runtime of nb by using the top .5 percent most discriminating terms.\n')

    # Run nb() with most discriminating terms
    nb(training_data, testing_data, df_file_path='df-mdt.csv')


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('train', help='Use for providing training data', type=str)
    parser.add_argument('test', help='Use for providing testing data', type=str)
    parser.add_argument('func', help='Provide a function. Options: tf, tfgrep, priors, df, mnb, nb, mine', type=str)
    args = parser.parse_args()
    function_map = {
        'tf': tf,
        'tfgrep': tfgrep,
        'priors': priors,
        'df': df,
        'mnb': mnb,
        'nb': nb,
        'mine': mine
    }
    if args.func in function_map:
        function_map[args.func](args.train, args.test, verbose=True)
